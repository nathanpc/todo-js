// db.js
// The "db" operation stuff

var fs = require("fs");


function parse_todo(lines) {
  var todo = [];
  var done = [];
  
  for (var i = 0; i < lines.length; i++) {
    if (lines[i] != "") {
      build_todo_item(lines[i], function (is_done, item) {
        if (is_done) {
          done.push(item);
        } else {
          todo.push(item);
        }
      });
    }
  }
  
  var json = {
    "todo": todo,
    "done": done
  };
  
  return json;
}

function build_todo_item(line, callback) {
  var item;

  var done;
  var title;
  var desc;
  
  if (line.substr(0, 2) === "x ") {
    done = true;
    title = line.split(" - ")[0].substr(2, line.split(" - ")[0].length);
  } else {
    done = false;
    title = line.split(" - ")[0];
  }
  
  if (line.split(" - ").length == 1) {
    desc = "";
  } else {
    desc = line.split(" - ")[1];
  }
  
  item = {
    "done": done,
    "title": title,
    "description": desc
  }
  
  callback(done, item);
}

module.exports = {
  get_items: function (location, callback) {
    var json = parse_todo(fs.readFileSync(location, "utf-8").split("\n"));
    
    callback(json.todo, json.done);
  }
};