// table.js
// Generating tables.

var Table = require("cli-table");

function gen_table(todo) {
  var table = new Table({
    head: [ "#", "Item" ]
  });

  for (var i = 0; i < todo.length; i++) {
    table.push([i, todo[i].title]);
  }

  return table;
}

module.exports = {
  show: function (todo) {
    return gen_table(todo).toString();
  }
};