// menu.js
// Menu controller.

var prompt = require("prompt");

var db = require("./db.js");
var table = require("./table.js");

function build_menu(options, actions) {
  for (var i = 0; i < options.length; i++) {
    console.log(i + " - " + options[i]);
  }

  // TODO: Find a way to make this not show the .name
  var property = {
    name: "option",
    message: "",
    validator: /^\d*$/,
    warning: "Must be a number!"
  };

  console.log("");

  prompt.message = "";
  prompt.delimiter = ">";
  prompt.start();

  prompt.get(property, function (err, result) {
    try {
      actions[result.option]();
    } catch(e) {
      console.log("Error: Please type a valid option.");
    }
  });
}

module.exports = {
  show: function (menu) {
    switch (menu) {
      case "main":
        var options = [ "List TODO", "List Done", "Quit" ];
        var actions = [
          function() {
            db.get_items("./TODO.lst", function (todo, done) {
              console.log(table.show(todo));
            });
            
            module.exports.show("todo_list");
          },
          function() {
            db.get_items("./TODO.lst", function (todo, done) {
              console.log(table.show(done));
            });
          },
          function() {
            process.exit(0);
          }
        ];

        build_menu(options, actions);
        break;
      case "todo_list":
        var options = [ "Add Item", "List Done", "Main Menu" ];
        var actions = [
          function() {
            db.get_items("./TODO.lst", function (todo, done) {
              console.log(table.show(todo));
            });
            
            
          },
          function() {
            db.get_items("./TODO.lst", function (todo, done) {
              console.log(table.show(done));
            });
            
            module.exports.show("done_list");
          },
          function() {
            module.exports.show("main");
          }
        ];
        
        build_menu(options, actions);
        break;
    }
  }
};